import { Validator, mongoose } from '../../../modules/index.js';
import {
    loginController,
    registerController,
    foodController,
    userController
} from '../../controllers/index.js';

export default {
    Query: {
        async listFavourite(parent, data, context) {
            try {
                let status = (new Validator({  ...context }, {
                    token: 'string|required'
                })).check();
                if (!status) {
                    return {
                        status: 401,
                        error: {
                            message: 'error',
                            code: 1
                        }
                    }
                }
                console.log('sssssssssssssss')
                if (!(await loginController.checkToken(data, context)).status == 0) {
                    return {
                        status: 401,
                        message: 'not authorized'
                    }
                }
                console.log((await foodController.listFavorite(data, context)))
                return {
                    ... (await foodController.listFavorite(data, context))
                }
            } catch (ex) {
                console.log(ex);
                return {
                    status: 500,
                    message: 'error happend'
                }
            }
        },
        async checkToken(parent, data, context) {
            try {
                let status = new Validator({ token: context.token }, {
                    token: 'string|required'
                }).check();
                if (!status) {
                    return {
                        status: 401,
                        error: {
                            message: 'error',
                            code: 1
                        }
                    }
                }
                let result = await loginController.checkToken(data, context);
                switch (result.status) {
                    case 0: {
                        return {
                            message: result.message,
                            status: 200
                        }
                    }
                    case -1: {
                        return {
                            message: result.message,
                            status: 500
                        }
                    }
                    case 1: {
                        return {
                            message: result.message,
                            status: 404
                        }
                    }
                    case 2: {
                        return {
                            message: 'user not found',
                            status: 404
                        }
                    }
                }
            } catch (ex) {

                return {
                    status: 500,
                    error: {
                        message: 'error happend',
                        code: 2
                    }
                }
            }
        },
        async listFood(parent, data, context) {
            try {
                let status = new Validator({ ...context }, {
                    token: 'string|required'
                }).check();

                if (!status) {
                    return {
                        status: 401,
                        message: 'bad input'
                    }
                }
                if (!(await loginController.checkToken(data, context)).status == 0) {
                    return {
                        status: 401,
                        message: 'not authorized'
                    }
                }
                let result = await foodController.listFood(data, context);

                return result;
            } catch (ex) {
                return {
                    status: 500,
                    message: 'error'
                }
            }
        },
        async listCategory(parent, data, context) {
            try {
                let status = new Validator({ ...context }, {
                    token: 'string|required'
                }).check();
                if (!status) {
                    return {
                        status: 401,
                        message: 'bad input'
                    }
                }
                if (!(await loginController.checkToken(data, context)).status == 0) {
                    console.log('sss')
                    return {
                        status: 401,
                        message: 'not authorized'
                    }
                }
                let result = await foodController.listCategory(data, context);
                return result;
            } catch (ex) {
                return {
                    status: 500,
                    message: 'error'
                }
            }
        },
        async listUser(parent, data, context) {
            try {
                let status = new Validator({ ...context }, {
                    token: 'string|required'
                }).check();
                if (!status) {
                    return {
                        status: 401,
                        message: 'bad input'
                    }
                }
                if (!(await loginController.checkToken(data, context)).status == 0) {
                    return {
                        status: 401,
                        message: 'not authorized'
                    }
                }
                let result = await userController.listUser(data, context);

                return result;
            } catch (ex) {
                return {
                    status: 500,
                    message: 'error'
                }
            }
        },
        async getProfile(parent, data, context) {
            try {
                let status = new Validator({ ...context }, {
                    token: 'string|required'
                }).check();
                if (!status) {
                    return {
                        status: 401,
                        message: 'bad input'
                    }
                }
                if (!(await loginController.checkToken(data, context)).status == 0) {
                    return {
                        status: 401,
                        message: 'not authorized'
                    }
                }
                let result = await userController.getProfile(data, context);
                console.log(result.data)
                return result;
            } catch (ex) {
                return {
                    status: 500,
                    message: 'error'
                }
            }
        }
    },
    Mutation: {
        async addFavourite(parent, data, context) {
            console.log(data)
            try {
                let status = (new Validator({ ...data, context }, {
                    id: 'string|required',
                    favorite: 'boolean|required',
                    token: 'string|required'
                })).check();
                console.log('--------------------------')
                if (!status) {
                    console.log('--------------------------')
                    return {
                        status: 401,
                        error: {
                            message: 'error',
                            code: 1
                        }
                    }
                }
                if (!(await loginController.checkToken(data, context)).status == 0) {
                    return {
                        status: 401,
                        message: 'not authorized'
                    }
                }
                return {
                    ... (await foodController.addFavorite(data, context))
                }
            } catch (ex) {
                console.log(ex);
                return {
                    status: 500,
                    message: 'error happend'
                }
            }
        },
        async deleteFavourite(parent, data, context) {
            try {
                let status = (new Validator({ ...data, ...context }, {
                    id: 'string|required',
                    token: 'string|required'
                })).check();
                if (!status) {
                    return {
                        status: 401,
                        error: {
                            message: 'error',
                            code: 1
                        }
                    }
                }
                if (!(await loginController.checkToken(data, context)).status == 0) {
                    return {
                        status: 401,
                        message: 'not authorized'
                    }
                }
                return {
                    ... (await foodController.deleteFavorite(data, context))
                }
            } catch (ex) {
                console.log(ex);
                return {
                    status: 500,
                    message: 'error happend'
                }
            }
        },
        async login(parent, data, context) {
            try {

                let status = (new Validator({ ...data, ...context}, {
                    email: 'string|required',
                    password: 'string|required'
                })).check();
                if (!status) {
                    return {
                        status: 401,
                        error: {
                            message: 'error',
                            code: 1
                        }
                    }
                }
                let result = await loginController.login(data, context);
                switch (result.status) {
                    case 0: {
                        return {
                            message: result.message,
                            status: 200,
                            token: result.token
                        }
                    }
                    case 1: {
                        return {
                            message: result.message,
                            status: 404
                        }
                    }
                    case 2: {
                        return {
                            message: result.message,
                            status: 400
                        }
                    }
                    case 3: {
                        return {
                            message: result.message,
                            status: 403
                        }
                    }
                    case -1: {
                        return {
                            message: result.message,
                            status: 500
                        }
                    }
                }

            } catch (ex) {

                console.log(ex);
                return {
                    status: 500,
                    message: 'error happend'
                }
            }
        },
        async register(parent, data, context) {
            try {
                let status = new Validator({ ...data }, {
                    email: 'string|required',
                    password: 'string|required'
                }).check();
                console.log(status);
                if (!status) {
                    return {
                        status: 400,
                        error: {
                            message: 'bad input',
                            code: 1
                        }
                    }
                }
                let result = await registerController.register(data, context);
                switch (result.status) {
                    case 0: {
                        return {
                            status: 200,
                            token: result.data.token
                        }
                    }
                    case -1: {
                        return {
                            status: 500,
                            message: 'error'
                        }
                    }
                    case 2: {
                        return {
                            status: 400,
                            message: 'user exeists'
                        }
                    }
                }
            } catch (ex) {
                console.log(ex);
                return {
                    status: 500,
                    error: {
                        message: 'error happend',
                        code: 2
                    }
                }
            }
        },
        async addFood(parent, data, context) {
            try {
                let status = new Validator({ ...data, ...context }, {
                    title: 'string|required',
                    carbs: 'string|required',
                    fat: 'string|required',
                    protein: 'string|required',
                    image: 'string|required',
                    token: 'string|required',
                    meal: 'string|required',
                    materials: 'string|required',
                    recipes: 'string|required',
                }).check();
                if (!status) {
                    return {
                        status: 400,
                        message: 'bad input'
                    }
                }
                if (!(await loginController.checkToken(data, context)).status == 0) {
                    return {
                        status: 401,
                        message: 'not authorized'
                    }
                }
                const result = await foodController.addFood(data, context);
                return {
                    status: result.status,
                    message: result.message
                }
            } catch (ex) {
                console.log(ex);
                return {
                    status: 500,
                    message: 'error happend'
                }
            }
        },
        async deleteFood(parent, data, context) {
            try {
                let status = new Validator({ ...data, ...context }, {
                    id: 'string|required',
                    token: 'string|required'
                }).check();
                if (!status || !mongoose.Types.ObjectId.isValid(data?.id)) {
                    return {
                        status: 401,
                        message: 'bad input'
                    }
                }

                let result = await foodController.deleteFood(data, context);
                return result;
            } catch (ex) {
                console.log(ex);
                return {
                    status: 500,
                    message: 'error happend'
                }
            }
        },
        async addCategory(parent, data, context) {
            try {
                let status = new Validator({ ...data, ...context }, {
                    title: 'string|required',
                    token: 'string|required'
                }).check();
                if (!status) {
                    return {
                        status: 401,
                        message: 'bad input'
                    }
                }
                let result = await foodController.addCategory(data, context);
                console.log('added');
                return result;
            } catch (ex) {
                return {
                    status: 500,
                    error: {
                        message: 'error happend'
                    }
                }
            }
        },
        async editFood(parent, data, context) {
            try {
                let status = new Validator({ ...data, ...context }, {
                    title: 'string|required',
                    carbs: 'string|required',
                    fat: 'string|required',
                    protein: 'string|required',
                    image: 'string',
                    token: 'string|required',
                    meal: 'string|required',
                    materials: 'string|required',
                    recipes: 'string|required',
                }).check();
                if (!status) {
                    return {
                        status: 401,
                        message: 'bad input'
                    }
                }
                if (!(await loginController.checkToken(data, context)).status == 0) {
                    return {
                        status: 401,
                        message: 'not authorized'
                    }
                }
                const result = await foodController.editFood(data, context);
                return {
                    status: result.status,
                    message: result.message
                }
            } catch (ex) {
                console.log(ex);
                return {
                    status: 500,
                    message: 'error'
                }
            }
        },
        async deleteCategory(parent, data, context) {
            try {
                let status = new Validator({ ...data, ...context }, {
                    id: 'string|required',
                    token: 'string|required'
                }).check();
                if (!status || !mongoose.Types.ObjectId.isValid(data?.id)) {
                    return {
                        status: 401,
                        message: 'bad input'
                    }
                }

                let result = await foodController.deleteCategory(data, context);
                return result;
            } catch (ex) {
                console.log(ex);
                return {
                    status: 500,
                    message: 'error happend'
                }
            }
        },
        async editProfile(parent, data, context) {
            try {
                let status = new Validator({ ...data, ...context }, {
                    firstname: 'string',
                    lastnmae: 'string',
                    image: 'string',
                    token: 'string',
                    token: 'string|required'
                }).check();
                if (!status) {
                    return {
                        status: 401,
                        message: 'bad input'
                    }
                }
                if (!(await loginController.checkToken(data, context)).status == 0) {
                    return {
                        status: 401,
                        message: 'not authorized'
                    }
                }
                const result = await userController.editProfile(data, context);
                return result;
            } catch (ex) {
                console.log(ex);
                return {
                    status: 500,
                    message: 'error'
                }
            }
        }
    }

}