import loginController from './auth/loginController.js';
import registerController from './auth/registerController.js';
import foodController from './foodController.js';
import userController from './userController.js';
export {
    loginController,
    registerController,
    foodController,
    userController
}