import Controller from '../controller.js';
import { } from '../../../modules/index.js';
import { token as tokenTools } from '../../../tools/index.js';
import { User } from '../../../models/index.js';
import { assertListType } from 'graphql';



export default new class extends Controller {
    constructor() {
        super();
    }
    async register(data, context) {
        try {
            let { email, password } = data;
            let user = await User.findOne({ email });
            console.log(data);
            if (user) {
                return {
                    message: 'user exists',
                    status: 2
                }
            }
            let newUser = new User({ email, password });
            let { id } = await newUser.save();

            let token = await tokenTools.create({ id, expire: 1000 * 60 * 60 * 24 * 365 * 10 });
            return {
                message: 'created',
                status: 0,
                data: {
                    token
                }
            }
        } catch (ex) {
            console.log(ex);
            return {
                message: 'error',
                status: -1
            }
        }
    }
}