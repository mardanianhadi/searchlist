import Controller from './controller.js';
import { fs } from '../../modules/index.js';
import { token as tokenTools } from './../../tools/index.js'
import { User } from '../../models/index.js';

export default new class extends Controller {
    constructor() {
        super();
    }
    async getProfile(data, context) {
        try {
            let { token } = context;
            let { id } = await tokenTools.verify(token);
            let user = await User.findById(id);
            delete user.password;
            if (!user) {
                return {
                    status: 404,
                    message: 'not found'
                }
            }

            return {
                status: 200,
                data: user
            }
        } catch (ex) {
            return {
                status: 500,
                message: 'error'
            }
        }
    }
    async editProfile(data, context) {
        try {
            let { image } = data;
            let { token } = context;
            let obj = { ...data };
            let payload = await tokenTools.verify(token);
            payload = payload ?? {};
            let { id: _id } = payload;
            let user = await User.findById(_id);
            if (!user) {
                return {
                    status: 404,
                    message: 'not found'
                }
            }

            if (image) {
                console.log('sssssssssss-------------------')
                let oldImage = user.image;
                if (oldImage) {
                    let oldPath = `./public${oldImage}`;
                    if (fs.existsSync(oldPath)) {
                        fs.unlinkSync(oldPath);
                    }
                }
                let ext = 'png';
                // for (let i = 0; image[i] != ';'; i++) {
                //     ext += image[i]
                // }
                // ext = ext.split('/')[1];


                let uploadPath = './public/upload/user';
                let filename = `${Date.now()}-${_id}.${ext}`;

                if (fs.existsSync(`${uploadPath}/${filename}`)) {
                    filename = `${uniqueString()}-${filename}`;
                }

                let fullPath = `${uploadPath}/${filename}`;
                fs.writeFileSync(fullPath, image, { encoding: 'base64' });
                if (fullPath) {
                    obj.image = fullPath.substring(8);
                }
            }else{
                delete obj.image;
            }
            if(!obj.firstname) delete obj.firstname;
            if(!obj.lastname) delete obj.lastname;
            if(!obj.phone) delete obj.phone;
            if(!obj.email) delete obj.email;
            if(!obj.password) delete obj.password;
            
            await user.updateOne({ ...obj });
            console.log('ddddddddddddddddddd')
            return {
                status: 200,
                message: 'updated success'
            }
        } catch (ex) {
            console.log(ex);
            return {
                status: 500,
                message: 'error'
            }
        }
    }
    async listUser(data, context){
        try{
            let users = await User.find({});
            return {
                status: 200,
                data: users
            }
        }catch(ex){
            console.log(ex);
            return {
                status: 500,
                message: 'error'
            }
        }
    }
}