import Controller from './controller.js';
import { fs, mongoose, uniqueString } from '../../modules/index.js';
import { token as tokenTools } from './../../tools/index.js'
import { Food, Category, Favourite, User } from '../../models/index.js';


export default new class extends Controller {
    constructor() {
        super();
    }
    decodeBase64Image(dataString) {
        var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
            response = {};

        if (matches.length !== 3) {
            return new Error('Invalid input string');
        }

        response.type = matches[1];
        response.data = new Buffer(matches[2], 'base64');

        return response;
    }
    async addFood(data, context) {
        try {
            let {
                title,
                image,
                body,
                category,
                meal,
                materials,
                recipes,
                carbs,
                protein,
                fat
            } = data;

            let obj = {
                title,
                body,
                category,
                meal,
                materials,
                recipes,
                carbs,
                protein,
                fat
            };
            if (mongoose.Types.ObjectId.isValid(category)) {
                let cat = await Category.findById(category);
                if (!cat)
                    delete obj.category
            } else {
                delete obj.category
            }
            let { token } = context;
            let ext = 'png';
            // for (let i = 0; image[i] != ';'; i++) {
            //     ext += image[i]
            // }
            // ext = ext.split('/')[1];

            let payload = await tokenTools.verify(token);
            payload = payload ?? {};
            let { id: _id } = payload;

            let uploadPath = './public/upload/food';
            let filename = `${Date.now()}-${_id}.${ext}`;

            if (fs.existsSync(`${uploadPath}/${filename}`)) {
                filename = `${uniqueString()}-${filename}`;
            }

            let fullPath = `${uploadPath}/${filename}`;
            fs.writeFileSync(fullPath, image, { encoding: 'base64' });
            obj.image = fullPath.substring(8);
            let food = new Food({ ...obj });
            await food.save();
            return {
                status: 0,
                message: 'created'
            }
        } catch (ex) {
            console.log(ex);
            return {
                status: -1,
                message: 'error'
            }
        }
    }

    async deleteFood(data, context) {
        try {
            let { id: _id } = data;

            let food = await Food.findById(_id);
            if (!food) {
                return {
                    status: 404,
                    message: 'not found'
                }
            }
            let image = `./public${food.image}`;
            if (fs.existsSync(image)) {
                fs.unlinkSync(image);
            }
            await food.deleteOne();
            return {
                status: 200,
                message: 'deleted success'
            }
        } catch (ex) {
            console.log(ex);
            return {
                status: -1,
                message: 'error'
            }
        }
    }
    async listFood(data, context) {
        try {
            let foods = await Food.find({});

            return {
                status: 200,
                data: foods
            };
        } catch (ex) {
            console.log(ex);
            return {
                status: -1,
                message: 'error'
            }
        }
    }
    async listCategory(data, context) {
        try {
            let cats = await Category.find({});
            return {
                status: 200,
                data: cats
            };
        } catch (ex) {
            console.log(ex);
            return {
                status: -1,
                message: 'error'
            }
        }
    }
    async editFood(data, context) {
        try {
            let {
                title,
                image,
                body,
                category,
                meal,
                materials,
                recipes,
                carbs,
                protein,
                fat,
                foodId
            } = data;

            let obj = {
                title,
                image,
                body,
                category,
                meal,
                materials,
                recipes,
                carbs,
                protein,
                fat
            };
            let { token } = context;
            let food = await Food.findById(foodId);
            if (!food) {
                return {
                    status: 404,
                    message: 'not found'
                }
            }

            if (image) {
                let oldImage = `./public${food.image}`;
                if (fs.existsSync(oldImage)) {
                    fs.unlinkSync(oldImage);
                }

                let ext = 'png';
                // for (let i = 0; image[i] != ';'; i++) {
                //     ext += image[i]
                // }
                // ext = ext.split('/')[1];

                let payload = await tokenTools.verify(token);
                payload = payload ?? {};
                let { id: _id } = payload;

                let uploadPath = './public/upload/food';
                let filename = `${Date.now()}-${_id}.${ext}`;

                if (fs.existsSync(`${uploadPath}/${filename}`)) {
                    filename = `${uniqueString()}-${filename}`;
                }

                let fullPath = `${uploadPath}/${filename}`;
                fs.writeFileSync(fullPath, image, { encoding: 'base64' });
                obj.image = fullPath.substring(8);
            }
            if (!obj.title) delete obj.title;
            if (!obj.image) delete obj.image;

            await food.updateOne({ ...obj });
            return {
                status: 200,
                message: 'edited'
            }
        } catch (ex) {
            console.log(ex);
            return {
                status: -1,
                message: 'error'
            }
        }
    }
    async addCategory(data, context) {
        try {
            let { title } = data;
            let cat = new Category({ title });
            await cat.save();
            return {
                status: 200,
                message: 'created'
            }


        } catch (ex) {
            console.log(ex);
            return {
                status: -1,
                message: 'error'
            }
        }
    }
    async deleteCategory(data, context) {
        try {
            let { id } = data;
            let category = await Category.findById(id);
            if (!category) {
                return {
                    status: 404,
                    message: 'not found'
                }
            }
            await category.remove();
            return {
                status: 200,
                message: 'delete success'
            }
        } catch (ex) {
            console.log(ex);
            return {
                status: -1,
                message: 'error'
            }
        }
    }
    async addFavorite(data, context) {
        try {
            let { id: _id, favorite } = data;
            let food = await Food.findById(_id);
            if (!food) return {
                status: 404,
                message: 'not found'
            }
            await food.updateOne({ favorite: !food.favorite })
            let { token } = context;
            let { id: userId } = await tokenTools.verify(token);

            let fav = await Favourite.findOne({ user: userId });
            
            if (fav) {
                if (favorite) {
                    if (!fav.food.includes(_id)) {
                        fav.food.push(_id)
                    }
                    await fav.save();
                } else {
                    fav.food = fav.food.filter(f => f != _id);
                    await fav.save();
                }

            } else {
                if(favorite){
                    fav = new Favourite({ user: userId, food: _id });
                    await fav.save();
                }
            }



            return {
                status: 200,
                message: 'created'
            }
        } catch (ex) {
            console.log(ex);
            return {
                message: 'status',
                code: 500
            }
        }
    }
    async deleteFavorite(data, context) {
        try {
            let { token } = context;
            let { id: _id } = await tokenTools.verify(token);
            let fav = await Favourite.findOne({ user: _id });
            if (!fav) return {
                status: 404,
                message: 'not found'
            }
            fav.food = fav.food.filter(f => f != data.id);
            await fav.save();
            return {
                status: 200,
                message: 'deleted'
            }
        } catch (ex) {
            console.log(ex);
            return {
                message: 'status',
                code: 500
            }
        }
    }

    async listFavorite(data, context) {
        try {
            let { token } = context;
            let { id: _id } = await tokenTools.verify(token);
            let favs = await Favourite.findOne({ user: _id }).populate([
                {
                    path: 'food'
                }
            ]);
            if (!favs) return {
                status: 404,
                message: 'not found'
            }
            return {
                status: 200,
                message: 'fetched',
                data: favs.food
            }
        } catch (ex) {
            console.log(ex);
            return {
                message: 'status',
                code: 500
            }
        }
    }
}